package com.example.josh.betachatandroid;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class MainActivity extends AppCompatActivity {

    Socket socket;
    ArrayList<String> messages;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    TextView inputTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ConnectionHelper connectionHelper = new ConnectionHelper();
        messages = new ArrayList<>();

        socket = connectionHelper.getSocket();
        socket.connect();
        socket.on("chat message", onNewMessage);
        socket.on(Socket.EVENT_CONNECT,onConnect);

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        inputTextView = (TextView) findViewById(R.id.input);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new MyAdapter(messages);
        mRecyclerView.setAdapter(mAdapter);
        //not sure about this
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

    void sendMessage(View v) {


        //socket.emit("chat message","this is coming from android");
        socket.emit("chat message", inputTextView.getText());
        inputTextView.setText("");

    }

    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //JSONObject data = (JSONObject) args[0];

                    //JSONArray dataArray = data.toJSONArray();
                    //whoa, socket.io is auto converting to a string for me
                    String s = (String) args[0];


                    Log.d("gnome", "onNewMessage string: " + s);
                    messages.add(s);
                    mAdapter.notifyItemInserted(messages.size() - 1);

                    mRecyclerView.scrollToPosition(mAdapter.getItemCount() - 1);


                    /*
                    String username;
                    String message;
                    try {
                        username = data.getString("username");
                        message = data.getString("message");
                    } catch (JSONException e) {
                        Log.e(TAG, e.getMessage());
                        return;
                    }
                    addMessage(username, message);

                */
                }
            });

        }
    };
    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    Toast.makeText(getApplicationContext(),
                            R.string.connect, Toast.LENGTH_LONG).show();

                }
            });
        }
    };
}
