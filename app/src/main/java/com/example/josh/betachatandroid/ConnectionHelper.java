package com.example.josh.betachatandroid;

import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;

/**
 * Created by josh on 7/31/2017.
 */

public class ConnectionHelper {
    private Socket mSocket;

    {
        try {
            mSocket = IO.socket("http://picasaur.us:8080/");
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public Socket getSocket() {
        return mSocket;
    }


}
